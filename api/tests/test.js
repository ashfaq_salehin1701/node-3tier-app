var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../bin/www');
var should = chai.should();
var expect = chai.expect;

chai.use(chaiHttp);

describe('API Test', function () {
  it('This request should be successful with 200', function (done) {
    chai.request(server)
      .get('/api/test')
      .end(function(err, res) {
        res.should.have.status(200);
        done();
      });
  });
});
