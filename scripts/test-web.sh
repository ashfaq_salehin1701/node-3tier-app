#!/bin/bash

docker run -p 8080:3000 node-web npm test

if [ $? -ne 0 ]; then
    exit 1
fi
