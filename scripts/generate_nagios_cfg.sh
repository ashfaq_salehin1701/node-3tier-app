#!/bin/bash

rm -rf ../provisioning/roles/nagios_setup/files/host-groups.cfg
rm -rf ../provisioning/roles/nagios_setup/files/hosts.cfg
touch ../provisioning/roles/nagios_setup/files/host-groups.cfg
touch ../provisioning/roles/nagios_setup/files/hosts.cfg

tier='jenkins'
members='jenkins1'
export tier
export members
group=$(envsubst < "host_group_template")

printf "$(cat ../provisioning/roles/nagios_setup/files/host-groups.cfg)\n$group" > ../provisioning/roles/nagios_setup/files/host-groups.cfg

tier='api'
members=''
export tier
for ((i=1;i<=API_INSTANCES_COUNT;i++));
do
  members+="${tier}${i},"
done

members=${members::-1}
export members

group=$(envsubst < "host_group_template")

printf "$(cat ../provisioning/roles/nagios_setup/files/host-groups.cfg)\n$group" > ../provisioning/roles/nagios_setup/files/host-groups.cfg

tier='web'
members=''
export tier
for ((i=1;i<=WEB_INSTANCES_COUNT;i++));
do
  members+="${tier}${i},"
done

members=${members::-1}
export members

group=$(envsubst < "host_group_template")

printf "$(cat ../provisioning/roles/nagios_setup/files/host-groups.cfg)\n$group" > ../provisioning/roles/nagios_setup/files/host-groups.cfg

tier='api'
export tier
for ((i=1;i<=API_INSTANCES_COUNT;i++));
do
  instance=${api_private_ips[$i - 1]}
  export i
  export instance
  hostcfg=$(envsubst < "host_cfg_template")
  echo -e "$(cat ../provisioning/roles/nagios_setup/files/hosts.cfg)\n\n$hostcfg" > ../provisioning/roles/nagios_setup/files/hosts.cfg
done

tier='web'
export tier
for ((i=1;i<=WEB_INSTANCES_COUNT;i++));
do
  instance=${web_private_ips[$i - 1]}
  export i
  export instance
  hostcfg=$(envsubst < "host_cfg_template")
  echo -e "$(cat ../provisioning/roles/nagios_setup/files/hosts.cfg)\n\n$hostcfg" > ../provisioning/roles/nagios_setup/files/hosts.cfg
done

tier='jenkins'
export tier
instance=${jenkins_private_ip}
i=1
export i
export instance
hostcfg=$(envsubst < "host_cfg_template")
echo -e "$(cat ../provisioning/roles/nagios_setup/files/hosts.cfg)\n\n$hostcfg" > ../provisioning/roles/nagios_setup/files/hosts.cfg
