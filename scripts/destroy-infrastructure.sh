#!/bin/bash

cd ../infrastructure/ && terraform destroy -auto-approve -var 'key_name=devops' -var 'public_key_path=/home/ashfaq/.ssh/id_rsa.pub'
if [ $? -ne 0 ]; then
    exit 1
fi
