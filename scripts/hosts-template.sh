echo '[jenkins]'
echo ${jenkins_instance}
echo '[elk]'
echo ${elk_public_ip}
echo '[nagios]'
echo ${nagios_public_ip}
echo '[api]'
for ((idx=0;idx<API_INSTANCES_COUNT;idx++));
do
  echo ${api_instances[$idx]}
done
echo '[web]'
for ((idx=0;idx<WEB_INSTANCES_COUNT;idx++));
do
  echo ${web_instances[$idx]}
done
