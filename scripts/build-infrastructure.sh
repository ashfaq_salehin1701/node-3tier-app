#!/bin/bash

source ./.env

cd ../infrastructure/ && terraform apply -auto-approve -var 'key_name=devops' -var 'public_key_path=/home/ashfaq/.ssh/id_rsa.pub' -var "api_instances_count=${API_INSTANCES_COUNT}" -var "web_instances_count=${WEB_INSTANCES_COUNT}"
if [ $? -ne 0 ]; then
    exit 1
fi
