#!/bin/bash

export SSH_KEYDIR="../../../../.ssh"

source /var/host-addresses.sh
cd ./provisioning && ansible-playbook -i hosts deploy-web.yml -e 'ansible_python_interpreter=/usr/bin/python3' -e "elk_private_ip=${elk_private_ip}" -e "nagios_private_ip=${nagios_private_ip}"
