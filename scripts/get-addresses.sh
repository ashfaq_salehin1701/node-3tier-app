#!/bin/bash

output=$(cd ../infrastructure/ && terraform output)
if [ $? -ne 0 ]; then
    exit 1
fi
count=0
while read -r line
do
  if [ $count -eq 0 ]; then
    IFS=\= arr=($line)
    api_address="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 1 ]; then
    IFS=\= arr=($line)
    addresses="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
    IFS=\, api_instances=($addresses)
    for ((idx=0;idx<API_INSTANCES_COUNT;idx++));
    do
      api_instances[$idx]="$(echo -e "${api_instances[$idx]}" | sed -e 's/^[[:space:]]*//')"
    done
  elif [ $count -eq 2 ]; then
    IFS=\= arr=($line)
    addresses="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
    IFS=\, api_private_ips=($addresses)
    for ((idx=0;idx<API_INSTANCES_COUNT;idx++));
    do
      api_private_ips[$idx]="$(echo -e "${api_private_ips[$idx]}" | sed -e 's/^[[:space:]]*//')"
    done
  elif [ $count -eq 3 ]; then
    IFS=\= arr=($line)
    db_instance="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 4 ]; then
    IFS=\= arr=($line)
    elk_private_ip="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 5 ]; then
    IFS=\= arr=($line)
    elk_public_ip="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 6 ]; then
    IFS=\= arr=($line)
    jenkins_instance="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 7 ]; then
    IFS=\= arr=($line)
    jenkins_private_ip="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 8 ]; then
    IFS=\= arr=($line)
    nagios_private_ip="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 9 ]; then
    IFS=\= arr=($line)
    nagios_public_ip="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 10 ]; then
    IFS=\= arr=($line)
    web_address="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
  elif [ $count -eq 11 ]; then
    IFS=\= arr=($line)
    addresses="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
    IFS=\, web_instances=($addresses)
    for ((idx=0;idx<WEB_INSTANCES_COUNT;idx++));
    do
      web_instances[$idx]="$(echo -e "${web_instances[$idx]}" | sed -e 's/^[[:space:]]*//')"
    done
  elif [ $count -eq 12 ]; then
    IFS=\= arr=($line)
    addresses="$(echo -e "${arr[1]}" | sed -e 's/^[[:space:]]*//')"
    IFS=\, web_private_ips=($addresses)
    for ((idx=0;idx<WEB_INSTANCES_COUNT;idx++));
    do
      web_private_ips[$idx]="$(echo -e "${web_private_ips[$idx]}" | sed -e 's/^[[:space:]]*//')"
    done
  fi
  let count++
done <<<"$output"
