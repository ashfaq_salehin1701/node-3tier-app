#!/bin/bash

source ./.env
source ./build-infrastructure.sh
cd ../scripts && source ./get-addresses.sh
if [ $? -ne 0 ]; then
    exit 1
fi

export db_instance
export api_address
export elk_private_ip
export nagios_private_ip
export elk_public_ip
export nagios_public_ip

envcontent=$(source ./hosts-template.sh)

rm -rf ../provisioning/hosts
touch ../provisioning/hosts
echo "$envcontent" > ../provisioning/hosts

hostAddresses=$(source ./host-addresses-template.sh)

rm -rf ../provisioning/roles/clone_repo/files/host-addresses.sh
touch ../provisioning/roles/clone_repo/files/host-addresses.sh
echo "$hostAddresses" > ../provisioning/roles/clone_repo/files/host-addresses.sh
chmod +x ../provisioning/roles/clone_repo/files/host-addresses.sh

apienvcontent=$(envsubst < ../api/.env.example)
rm -rf ../provisioning/roles/clone_repo/files/.env-api
touch ../provisioning/roles/clone_repo/files/.env-api
echo "$apienvcontent" > ../provisioning/roles/clone_repo/files/.env-api

webenvcontent=$(envsubst < ../web/.env.example)
rm -rf ../provisioning/roles/clone_repo/files/.env-web
touch ../provisioning/roles/clone_repo/files/.env-web
echo "$webenvcontent" > ../provisioning/roles/clone_repo/files/.env-web
export SSH_KEYDIR="$HOME/.ssh"

source ./generate_nagios_cfg.sh
cd ../provisioning && ansible-playbook -i hosts main.yml -e 'ansible_python_interpreter=/usr/bin/python3' -e "elk_private_ip=${elk_private_ip}" -e "nagios_private_ip=${nagios_private_ip}"
if [ $? -ne 0 ]; then
  echo "Error occured while configuring servers"
  exit 1
fi
