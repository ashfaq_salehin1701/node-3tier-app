output "api_address" {
  value = "${aws_elb.api.dns_name}"
}

output "web_address" {
  value = "${aws_elb.web.dns_name}"
}

output "api_instances" {
  value = "${join(", ", aws_instance.api.*.public_ip)}"
}

output "api_private_ips" {
  value = "${join(", ", aws_instance.api.*.private_ip)}"
}

output "web_instances" {
  value = "${join(", ", aws_instance.web.*.public_ip)}"
}

output "web_private_ips" {
  value = "${join(", ", aws_instance.web.*.private_ip)}"
}

output "db_instance" {
  value = "${aws_db_instance.db.address}"
}

output "jenkins_instance" {
  value = "${aws_instance.jenkins.public_ip}"
}

output "jenkins_private_ip" {
  value = "${aws_instance.jenkins.private_ip}"
}

output "elk_public_ip" {
  value = "${aws_instance.elk.public_ip}"
}

output "elk_private_ip" {
  value = "${aws_instance.elk.private_ip}"
}

output "nagios_public_ip" {
  value = "${aws_instance.nagios.public_ip}"
}

output "nagios_private_ip" {
  value = "${aws_instance.nagios.private_ip}"
}
