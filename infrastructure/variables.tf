variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.

Example: ~/.ssh/terraform.pub
DESCRIPTION
}

variable "key_name" {
  description = "Desired name of AWS key pair"
}

variable "api_instances_count" {
  description = "Number of API instances to be launched"
  default="2"
}

variable "web_instances_count" {
  description = "Number of Web instances to be launched"
  default="2"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-west-2"
}

variable "availability_zone_1" {
  description = "Availability zone 1 for db."
  default     = "us-west-2a"
}

variable "availability_zone_2" {
  description = "Availability zone 2 for db."
  default     = "us-west-2b"
}

# Ubuntu 18.04 LTS (x64)
variable "aws_amis" {
  default = {
    eu-west-1 = "ami-00035f41c82244dab"
    us-east-1 = "ami-0ac019f4fcb7cb7e6"
    us-east-2 = "ami-0f65671a86f061fcd"
    us-west-1 = "ami-063aa838bd7631e0b"
    us-west-2 = "ami-0bbe6b35405ecebdb"
  }
}
